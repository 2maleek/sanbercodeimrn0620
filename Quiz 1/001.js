function bandingkan(num1, num2) {
    if(num1 < 0 || num2 < 0 || num1 === num2) {
        return -1
    }else if(!num2) {
        return 1
    }else if( num1 > num2) {
        return num1
    }else if( num2 > num1) {
        return num2
    }
}

function balikString(str) {
    var result = ""
    for (let i = str.length-1; i >= 0; i--) {
        result += str[i]
    }
    return result
}

function palindrome(str) {
    var lowerStr = str.toLowerCase()
    var result = ""
    for (let i = lowerStr.length-1; i >= 0; i--) {
        result += lowerStr[i]
    }
    if(result === lowerStr) {
        return true
    } 
    return false
}

// TEST CASES Bandingkan Angka
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18

// TEST CASES BalikString
console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah

// TEST CASES Palindrome
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false