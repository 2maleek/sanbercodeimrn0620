console.log("LOOPING PERTAMA")
var flag = 0

while(flag < 20) {
    flag+=2
    console.log(flag + " - I love coding")
}

console.log("LOOPING KEDUA")
while(flag >= 2) {
    console.log(flag + " - I will become a mobile developer")
    flag-= 2
}

console.log("LOOPING KETIGA")
for (var i = 1; i <= 20; i++) {
    if(i % 3 === 0 && i % 2 !== 0) {
        console.log(i + " - I love coding")
    }
    else if(i % 2 !== 0) {
        console.log(i + " - Santai")
    }else {
        console.log(i + " - Berkualitas")
    }
}

console.log("LOOPING KEEMPAT")
for (var i = 0; i < 4; i++) {
    var temp = ""
    for (var j = 0; j < 8; j++) {
        temp += "#"
    }
    console.log(temp)
}

console.log("LOOPING KELIMA")
for (var i = 0; i <= 7; i++) {
    var temp = ""
    for (var j = 0; j < i; j++) {
        temp += "#"
    }
    console.log(temp)
}

console.log("LOOPING KEENAM")
for (var i = 0; i < 8; i++) {
    var temp = ""
    for (var j = 0; j < 8; j++) {
        if(i % 2 !== 0 && j % 2 === 0 ) {
            temp += "#"
        }else if(i % 2 === 0 && j % 2 !== 0){
            temp += "#"
        }else {
            temp += " "
        }
    } 
    console.log(temp)
}


