console.log("|NO 1|");
function range(startNum, finishNum) {
	let arr = [];
	if (!startNum || !finishNum) {
		return -1;
	} else if (startNum > finishNum) {
		for (let i = startNum; i >= finishNum; i--) {
			arr.push(i);
		}
	} else {
		for (let i = startNum; i <= finishNum; i++) {
			arr.push(i);
		}
	}
	return arr;
}
console.log(range(1, 10));
console.log(range(11, 18));
console.log(range(54, 50));
console.log(range());

console.log("\n|NO 2|");
function rangeWithStep(startNum, finishNum, step) {
	var arr = [];
	if (startNum > finishNum) {
		while (startNum >= finishNum) {
			arr.push(startNum);
			startNum -= step;
		}
	} else {
		while (startNum <= finishNum) {
			arr.push(startNum);
			startNum += step;
		}
	}
	return arr;
}
console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));

console.log("\n|NO 3|");
function sum(startNum, finishNum, step = 1) {
	var result = 0;
	if (!startNum) {
		return 0;
	} else if (!finishNum) {
		return startNum;
	} else if (startNum > finishNum) {
		while (startNum >= finishNum) {
			result += startNum;
			startNum -= step;
		}
	} else {
		while (startNum <= finishNum) {
			result += startNum;
			startNum += step;
		}
	}
	return result;
}
console.log(sum(1, 10));
console.log(sum(5, 50, 2));
console.log(sum(15, 10));
console.log(sum(20, 10, 2));
console.log(sum(1));
console.log(sum());

console.log("\n|NO 4|");
function dataHandling(mdArray) {
	mdArray.forEach(data => {
		console.log(
			`Nomor ID: ${data[0]}\nNama Lengkap: ${data[1]}\nTTL : ${data[2]} ${data[3]}\nHobi : ${data[4]}\n`
		);
	});
}
var input = [
	["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
	["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
	["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
	["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];
dataHandling(input);

console.log("\n|NO 5|");
function balikKata(word) {
	var balik = "";
	for (var i = word.length - 1; i >= 0; i--) {
		balik += word[i];
	}
	return balik;
}
console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I

console.log("\n|NO 6|");
var input = [
	"0001",
	"Roman Alamsyah ",
	"Bandar Lampung",
	"21/05/1989",
	"Membaca"
];
function dataHandling2(arr) {
	arr.splice(1, 1, "Roman Alamsyah Elsharawy");
	arr.splice(2, 1, "Provinsi Bandar Lampung");
	arr.splice(4, 0, "Pria");
	arr.splice(5, 1, "SMA Internasional Metro");
	console.log(arr);

	var tgl = arr[3].split("/");
	switch (Number(tgl[1])) {
		case 1:
			console.log("Januari");
			break;
		case 2:
			console.log("Februari");
			break;
		case 3:
			console.log("Maret");
			break;
		case 4:
			console.log("April");
			break;
		case 5:
			console.log("Mei");
			break;
		case 6:
			console.log("Juni");
			break;
		case 7:
			console.log("Juli");
			break;
		case 8:
			console.log("Agustus");
			break;
		case 9:
			console.log("September");
			break;
		case 10:
			console.log("Oktober");
			break;
		case 11:
			console.log("November");
			break;
		case 12:
			console.log("Desember");
			break;
	}

	sorted = tgl.slice();
	sorted.sort(function (a, b) {
		return b - a;
	});
	console.log(sorted);

	console.log(tgl.join("-"));
	console.log(arr[1].slice(0, 15));
}
dataHandling2(input);

/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */
