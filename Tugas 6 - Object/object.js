function arrayToObject(arr) {
	// Code di sini 
	console.log(arr)
	if(arr.length === 0) {
		console.log("")
	}
	var keyPeople = ["firstName", "lastName", "gender", "age"]
	for (let i = 0; i < arr.length; i++) {
		var objPeople = {}
		var now = new Date().getFullYear()
		for (let j = 0; j < arr[i].length; j++) {
			objPeople[keyPeople[j]] = arr[i][j]
		}
		if(typeof objPeople.age !== "number" || objPeople.age > now) {
			objPeople.age = "Invalid birthyear"
		}else {
			objPeople.age = now - objPeople.age
		}
		var fullName = objPeople.firstName + " " + objPeople.lastName
    console.log(i+1 + ". " + fullName + ": "+ JSON.stringify(objPeople))
	}
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
	1. Bruce Banner: { 
			firstName: "Bruce",
			lastName: "Banner",
			gender: "male",
			age: 45
	}
	2. Natasha Romanoff: { 
			firstName: "Natasha",
			lastName: "Romanoff",
			gender: "female".
			age: "Invalid Birth Year"
	}
*/

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
	1. Tony Stark: { 
			firstName: "Tony",
			lastName: "Stark",
			gender: "male",
			age: 40
	}
	2. Pepper Pots: { 
			firstName: "Pepper",
			lastName: "Pots",
			gender: "female".
			age: "Invalid Birth Year"
	}
*/

// Error case 
arrayToObject([]) // ""


function shoppingTime(memberId, money) {
	// you can only write your code here!
	var items = [
		['Sepatu Stacattu', 1500000],
    ['Baju Zoro', 500000],
    ['Baju H&N', 250000],
    ['Sweater Uniklooh', 175000],
		['Casing Handphone', 50000 ]
	]
	var listPurchased = []
	var changeMoney = money
	if(!memberId) {
		return "Mohon maaf, toko X hanya berlaku untuk member saja"
	}else if( money < 50000) {
		return "Mohon maaf, uang tidak cukup"
	}else {
		for (let i = 0; i < items.length; i++) {
			if(changeMoney >= items[i][1]) {
				listPurchased.push(items[i][0])
				changeMoney -= items[i][1]
			}
		}
	}
	return {
		memberId,
		money,
		listPurchased,
		changeMoney
	}
}
 
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

function naikAngkot(arrPenumpang) {
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
	//your code here
	if(arrPenumpang.length === 0) return []
	var result = []
	for (let i = 0; i < arrPenumpang.length; i++) {
		var penumpang = arrPenumpang[i][0]
		var naikDari  = arrPenumpang[i][1]
		var tujuan = arrPenumpang[i][2]
		var bayar = 0
		for(var j = rute.indexOf(naikDari); j < rute.length; j++) {
			if(tujuan === rute[j]) {
				break
			}
			bayar += 2000 
		}
		result.push({
			penumpang,
			naikDari,
			tujuan,
			bayar
		})
	}
	return result
}
 
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]