import React from "react";
import { StyleSheet, Text, View, ScrollView } from 'react-native';

import Aplikasi from "./Tugas/Tugas12/App";
import LoginScreen from './Tugas/Tugas13/LoginScreen'
import AboutScreen from './Tugas/Tugas13/AboutScreen'
import Quiz3 from './Quiz3/index'

export default function App() {
  return (
    // <Aplikasi />;
    // <LoginScreen />
    <AboutScreen />
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  }
});
